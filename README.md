# Marlin 1.1.9

## Current Configuration
- Trianglelabs E3D v6
- HallON v3
- HeroMe (previously Bullseye)
- MKS GEN L with TMC2208 on X, Y, Z and LV8709 for extruder
- Trianglelabs "Capricorn"
- Aluminium extruder

## Configure

### Linear advance

Enabled by uncommenting this line in *Configuration_adv.h*

```
#define LIN_ADVANCE
```

[Kalibracja linear advance/pressure advance krótkie how-to](https://reprapy.pl/viewtopic.php?f=8&t=3165&sid=3cb6ca31eb6b1cc2695e1b75e60e70e1)
[K-factor Calibration Pattern](http://marlinfw.org/tools/lin_advance/k-factor.html)

### Extruder cooling FAN

You have two options for connecting extruder fan:
- Directly for 24V 
- In **HE1**.

With the second option you can control fan speed and automatically turn on when heatblock will reach certain temperature.

I have enablied this by

```
#define E0_AUTO_FAN_PIN 7
```

Speed is 100%

```
#define EXTRUDER_AUTO_FAN_SPEED   255  // == 100% speed
```

If you want to disable this feature simply change **E0_AUTO_FAN_PIN** to **-1**

## Settings

```
Steps per unit:
  M92 X80.00 Y79.80 Z400.00 E99.45
Maximum feedrates (units/s):
  M203 X500.00 Y500.00 Z5.00 E60.00
Maximum Acceleration (units/s2):
  M201 X2000 Y2000 Z100 E5000
Acceleration (units/s2): P<print_accel> R<retract_accel> T<travel_accel>
  M204 P500.00 R1000.00 T500.00
Advanced: B<min_segment_time_us> S<min_feedrate> T<min_travel_feedrate> X<max_x_jerk> Y<max_y_jerk> Z<max_z_jerk> E<max_e_jerk>
  M205 B20000 S0.00 T0.00 X20.00 Y20.00 Z0.30 E5.00
PID settings for 200
  M301 P12.54 I0.79 D49.58
```